import Link from "next/link"
import { Resume } from "@prisma/client"

import { formatDate } from "@/lib/utils"
import { Skeleton } from "@/components/ui/skeleton"
import { ResumeOperations } from "@/components/resume-operations"

interface ResumeItemProps {
  resume: Pick<Resume, "id" | "title" | "createdAt">
}

export function ResumeItem({ resume }: ResumeItemProps) {
  return (
    <div className="flex items-center justify-between p-4">
      <div className="grid gap-1">
        <Link
          href={`/editor/${resume.id}`}
          className="font-semibold hover:underline"
        >
          {resume.title}
        </Link>
        <div>
          <p className="text-sm text-muted-foreground">
            {formatDate(resume.createdAt?.toDateString())}
          </p>
        </div>
      </div>
      <ResumeOperations resume={{ id: resume.id, title: resume.title }} />
    </div>
  )
}

ResumeItem.Skeleton = function ResumeItemSkeleton() {
  return (
    <div className="p-4">
      <div className="space-y-3">
        <Skeleton className="h-5 w-2/5" />
        <Skeleton className="h-4 w-4/5" />
      </div>
    </div>
  )
}
