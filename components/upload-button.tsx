import { Button } from "@/components/ui/button"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { pdfToText } from 'pdf-ts';
import { Input } from "@/components/ui/input"
import { Icons } from "@/components/icons"
import { toast } from "@/components/ui/use-toast"
import { useRouter } from "next/navigation"
import * as React from "react"

async function onUploadResume(resume) {
    const fileInput = document.getElementById("resume-file") as HTMLInputElement;

    if (!fileInput.files || fileInput.files.length === 0) {
		toast({title: "Empty!", description: "No file uploaded.", variant: 'destructive'})
        return;
    }

    const file = fileInput.files[0];
    try {
        const arrayBuffer = await file.arrayBuffer();
        const text = await pdfToText(arrayBuffer);

		const blocks = {
			"time": Math.floor(Date.now() / 1000),
			"blocks": [{"id":"5ncUumFt3-","data":{"text": text},"type":"paragraph"}],
			"version": "2.26.5"
		}

		const response = await fetch(`/api/resumes/${resume.id}`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				title: resume.title,
				content: blocks,
			}),
		})

		if (!response?.ok) {
		return toast({
			title: "Something went wrong.",
			description: "Your resume was not saved. Please try again.",
			variant: "destructive",
		})
		}

		return text
    } catch (error) {
        console.error("Error processing the file:", error);
    }
}

export function UploadResumeButton({ resume }) {
	const [open, setOpen] = React.useState<boolean>(false)
	const router = useRouter()

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button type="button" variant="outline">
			<span className="flex items-center">Upload {<Icons.post className="ml-1 h-4 w-4" />}</span>
		</Button>

      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Upload Resume</DialogTitle>
          <DialogDescription>
            Already have a resume? You can load it to the editor!
          </DialogDescription>
        </DialogHeader>
		<div className="grid w-full max-w-sm items-center gap-1.5">
		<Input id="resume-file" type="file" />
		</div>
        <DialogFooter>
          <Button type="button" onClick={async () => {
			let content = await onUploadResume(resume);
			setOpen(false)
			router.refresh()
			toast({title: "Success!", description: "Loaded in your resume."})
		  }} >Upload</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}