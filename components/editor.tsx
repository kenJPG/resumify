"use client"

import { redirect } from "next/navigation"
import * as React from "react"
import Link from "next/link"
import { useRouter } from "next/navigation"
import EditorJS from "@editorjs/editorjs"
import { zodResolver } from "@hookform/resolvers/zod"
import { Resume } from "@prisma/client"
import { useForm } from "react-hook-form"
import TextareaAutosize from "react-textarea-autosize"
import * as z from "zod"

import "@/styles/editor.css"
import { cn } from "@/lib/utils"
import { resumePatchSchema } from "@/lib/validations/resume"
import { buttonVariants } from "@/components/ui/button"
import { toast } from "@/components/ui/use-toast"
import { Icons } from "@/components/icons"
import { UploadResumeButton } from "@/components/upload-button"

interface EditorProps {
  resume: Pick<Resume, "id" | "title" | "content">
}

type FormData = z.infer<typeof resumePatchSchema>

export function Editor({ resume }: EditorProps) {
  const { register, handleSubmit } = useForm<FormData>({
    resolver: zodResolver(resumePatchSchema),
  })
  const ref = React.useRef<EditorJS>()
  const router = useRouter()
  const [isSaving, setIsSaving] = React.useState<boolean>(false)
  const [isApplying, setIsApplying] = React.useState<boolean>(false)
  const [isMounted, setIsMounted] = React.useState<boolean>(false)

  const initializeEditor = React.useCallback(async () => {
    const EditorJS = (await import("@editorjs/editorjs")).default
    const Header = (await import("@editorjs/header")).default
    const Embed = (await import("@editorjs/embed")).default
    const Table = (await import("@editorjs/table")).default
    const List = (await import("@editorjs/list")).default
    const Code = (await import("@editorjs/code")).default
    const LinkTool = (await import("@editorjs/link")).default
    const InlineCode = (await import("@editorjs/inline-code")).default

    const body = resumePatchSchema.parse(resume)

    if (!ref.current) {
      const editor = new EditorJS({
        holder: "editor",
        onReady() {
          ref.current = editor
        },
        placeholder: "Type here to start your resume...",
        inlineToolbar: true,
        data: body.content,
        tools: {
          header: Header,
          linkTool: LinkTool,
          list: List,
          code: Code,
          inlineCode: InlineCode,
          table: Table,
          embed: Embed,
        },
      })
    }
  }, [resume])

  React.useEffect(() => {
    if (typeof window !== "undefined") {
      setIsMounted(true)
    }
  }, [])

  React.useEffect(() => {
    if (isMounted) {
      initializeEditor()

      return () => {
        ref.current?.destroy()
        ref.current = undefined
      }
    }
  }, [isMounted, initializeEditor])

  async function onSubmit(data: FormData) {
    setIsSaving(true)

    const blocks = await ref.current?.save()

    const response = await fetch(`/api/resumes/${resume.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title: data.title,
        content: blocks,
      }),
    })

    setIsSaving(false)

    if (!response?.ok) {
      return toast({
        title: "Something went wrong.",
        description: "Your resume was not saved. Please try again.",
        variant: "destructive",
      })
    }

    router.refresh()

    return toast({
      description: "Your resume has been saved.",
    })
  }

  if (!isMounted) {
    return null
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="grid w-full gap-10">
        <div className="flex w-full items-center justify-between">
          <div className="flex items-center space-x-10">
            <Link
              href="/dashboard"
              className={cn(buttonVariants({ variant: "ghost" }))}
            >
              <>
                <Icons.chevronLeft className="mr-2 h-4 w-4" />
                Back
              </>
            </Link>
          </div>
        </div>
        <div className="prose prose-stone mx-auto w-[800px] dark:prose-invert">
          <TextareaAutosize
            autoFocus
            id="title"
            defaultValue={resume.title}
            placeholder="Resume title"
            className="w-full resize-none appearance-none overflow-hidden bg-transparent text-5xl font-bold focus:outline-none"
            {...register("title")}
          />
          <div id="editor" className="min-h-[500px]" />

			<div className="flex justify-between items-center">
				<UploadResumeButton resume = {resume} />
				{/* <button type="button" className={cn(buttonVariants({variant: 'outline'}))}>
					<span className="flex items-center">Upload {<Icons.post className="ml-1 h-4 w-4" />}</span>
				</button> */}

				<div className="flex gap-[10px]">
					<button type="submit" className={cn(buttonVariants({variant: 'secondary'}))}>
						{isSaving && (
							<Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
						)}
					<span>Save</span>
					</button>
					<button onClick={() => {
						setIsApplying(true);
						console.log(`redirecting to ${resume.id}`);
						redirect(`/apply/${resume.id}`);
					}} type="button" className={cn(buttonVariants())}>
						{isApplying && (
							<Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
						)}
					<span className="flex items-center">Apply {<Icons.arrowRight className="ml-1 h-4 w-4" />}</span>
					</button>
				</div>
			</div>
        </div>
      </div>
    </form>
  )
}
