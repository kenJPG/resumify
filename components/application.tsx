"use client"

import { notFound, redirect } from "next/navigation"
import React from 'react';
import { Button } from "@/components/ui/button";
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from "@/components/ui/card";
import { Textarea } from "@/components/ui/textarea";

export function ResumeSummary({ summary }) {
  return (
    <Card className="h-full p-1 bg-white shadow-lg rounded-xl">
      <CardHeader>
        <CardTitle className="text-xl font-bold text-gray-700">Resume Overview</CardTitle>
        <CardDescription className="text-sm text-gray-500">Here's a brief overview of what you have done</CardDescription>
      </CardHeader>
      <CardContent>
        <Textarea className="resize-none h-40" defaultValue={summary.experience} />
      </CardContent>
    </Card>
  );
}

export function Application({ summary }) {
  return (
    <Card className="h-full p-1 bg-white shadow-lg rounded-xl">
      <CardHeader>
        <CardTitle className="text-xl font-bold text-gray-700">Capabilities</CardTitle>
        <CardDescription className="text-sm text-gray-500">Here's what we got from your resume. Feel free to adjust!</CardDescription>
      </CardHeader>
      <CardContent>
        <Textarea className="h-40" defaultValue={summary.capability} />
      </CardContent>
      <CardFooter className="flex justify-between">
        <Button variant="outline">Cancel</Button>
        <Button onClick={() => {
			redirect('/results')
		}}>Search</Button>
      </CardFooter>
    </Card>
  );
}

function Grade({ grade }) {
  const gradeColors = {
    A: 'text-green-500',
    B: 'text-yellow-500',
    C: 'text-red-500'
  };
  return <div className={`text-3xl font-bold`}>Grade: <span className={gradeColors[grade]}>{grade}</span></div>;
}

function Comment({ comment }) {
  return (
    <div className="text-center text-lg font-semibold w-64 mx-auto">
      {comment}
    </div>
  );
}

export function MainPage({ summary }) {
	let grade = "F";
	try {
		grade = summary.grade.match(/[ABCDEF]/)[0];
	} catch {
	}
  const comment = summary.comment;

  return (
    <div className="grid grid-cols-3 gap-6 p-3">
      <div className="col-span-1 flex flex-col items-center space-y-4">
        <img src="/images/avatars/shadcn.png" alt="Profile" className="w-40 h-40 rounded-full shadow-lg" />
        <Grade grade={grade} />
        <Comment comment={comment} />
      </div>
      <div className="col-span-2 grid grid-rows-2 gap-6">
        <div className="row-span-1">
          <ResumeSummary summary={summary} />
        </div>
        <div className="row-span-1">
          <Application summary={summary} />
        </div>
      </div>
    </div>
  );
}
