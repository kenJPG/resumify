import * as z from "zod"

export const userAuthSchema = z.object({
  email: z.string().email(),
  password: z.string().min(4)
})

export const userRegSchema = z.object({
  name: z.string().min(3),
  email: z.string().email(),
  password: z.string().min(4)
})