import { PrismaAdapter } from "@next-auth/prisma-adapter"
import { NextAuthOptions } from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import EmailProvider from "next-auth/providers/email"
import GitHubProvider from "next-auth/providers/github"
import { Client } from "postmark"

import { env } from "@/env.mjs"
import { siteConfig } from "@/config/site"
import { db } from "@/lib/db"

export const authOptions: NextAuthOptions = {
  // huh any! I know.
  // This is a temporary fix for prisma client.
  // @see https://github.com/prisma/prisma/issues/16117
  adapter: PrismaAdapter(db as any),
  session: {
    strategy: "jwt",
  },
  pages: {
    signIn: "/login",
  },
  providers: [
	CredentialsProvider({
		// The name to display on the sign in form (e.g. "Sign in with...")
		id: 'signin',
		name: "credentials",
		// `credentials` is used to generate a form on the sign in page.
		// You can specify which fields should be submitted, by adding keys to the `credentials` object.
		// e.g. domain, username, password, 2FA token, etc.
		// You can pass any HTML attribute to the <input> tag through the object.
		credentials: {
			email: { label: "Email", type: "text" },
			password: { label: "Password", type: "password" }
		},
		async authorize(credentials, req) {
		// Add logic here to look up the user from the credentials supplied
			console.log("Credentials:", credentials, req)

			try {
				const user = await db.user.findFirst({
					where: {
						email: credentials?.email,
						password: credentials?.password
					}
				})

				console.log("USER:", user)

				return user
			} catch (error) {
				console.log("Error:", error)
				throw new Error("Something went wrong")
			}
		}
	}),
    CredentialsProvider({
        id: "signup",
        type: "credentials",
        credentials: {
			name: { label: "Name", type: "text" },
			email: { label: "Email", type: "text" },
			password: { label: "Password", type: "password" }
		},
        name: 'credentials',
        async authorize(credentials: any, req) {
            const payload = {
                name: credentials.name,
                email: credentials.email,
				password: credentials.password
            }

            // api call to backend
			try {
				const res = await db.user.create({
					data: payload
				})

				return {
					ok: true,
					...res
				}

			} catch (error) {
				console.log("Error", error)
				if (error.code == 'P2002') {
					throw new Error('This email is already used')
				}
				throw new Error('Something went wrong')
			}
        }
    })
  ],
  callbacks: {
    async session({ token, session }) {
      if (token) {
        session.user.id = token.id
        session.user.name = token.name
        session.user.email = token.email
        session.user.image = token.picture
      }

      return session
    },
    async jwt({ token, user }) {
      const dbUser = await db.user.findFirst({
        where: {
          email: token.email,
        },
      })

      if (!dbUser) {
        if (user) {
          token.id = user?.id
        }
        return token
      }

      return {
        id: dbUser.id,
        name: dbUser.name,
        email: dbUser.email,
        picture: dbUser.image,
      }
    },
  },
}
