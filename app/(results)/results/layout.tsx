interface ResultsProps {
  children?: React.ReactNode
}

export default function ResultsLayout({ children }: ResultsProps) {
  return (
    <div className="container mx-auto grid items-start gap-10 py-8">
		{children}
    </div>
  )
}