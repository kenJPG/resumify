import { Skeleton } from "@/components/ui/skeleton"

export default function Loading() {
  return (
    <div className="w-full mt-[40vh]">
		<div className="flex items-center justify-center space-x-2 animate-pulse">
			<div className="w-8 h-8 bg-orange-400 rounded-full"></div>
			<div className="w-8 h-8 bg-orange-500 rounded-full"></div>
			<div className="w-8 h-8 bg-orange-600 rounded-full"></div>
		</div>
		<div className="flex items-center justify-center">
			<div className="mt-3 text-lg font-semibold text-gray-700">
				Finding your perfect fit...
			</div>
		</div>
    </div>
  )
}