"use client"

import React from 'react';
import { useState, useEffect } from 'react';

// Assuming logos are stored in your public directory under /logos
const jobs = [
  {
    company: "HoneyWell",
    position: "Intern Bachelors Cyber Security",
    description: "Assist in creating cyber security protocols.",
    deiScore: 92, // Great match
    logo: "/images/blog/honeywell.png",
    moreInfoLink: "https://resumify-green.vercel.app/job-listing/application/ea6ce700-4b6a-4095-a713-d04ee64275dc",
    matches: true
  },
  {
    company: "Thales",
    position: "System Design Engineer Intern",
    description: "Design system architecture for next-gen products.",
    deiScore: 89, // Great match
    logo: "/images/blog/thales.png",
    moreInfoLink: "#",
    matches: true
  },
  {
    company: "Circle's Life",
    position: "Strategic Implementation Intern (CTO Office)",
    description: "Participate in strategic project implementations.",
    deiScore: 72, // Good match
    logo: "/images/blog/circles.png",
    moreInfoLink: "#",
    matches: true
  },
  {
    company: "Razer Inc",
    position: "Software (Web App) Intern",
    description: "Develop cutting-edge web applications.",
    deiScore: 95, // Great match
    logo: "/images/blog/razer.png",
    moreInfoLink: "#",
    matches: true
  },
  {
    company: "Integrated Health Information Systems",
    position: "Digital Media Project for New Hospital IHiS",
    description: "Assist the PM in major HealthTech projects and learn valuable industry skills.",
    deiScore: 85, // Adjust the score as needed
    logo: "/images/blog/ihis.png", // Replace with the actual path to the logo
    moreInfoLink: "#", // Link to the IHiS video
    matches: true
  },
  {
    company: "Rohde & Schwarz",
    position: "Intern, Test Developer",
    description: "Build and maintain test automation for software and hardware systems.",
    deiScore: 68, // Adjust the score as needed
    logo: "/images/blog/rohde.png", // Replace with the actual path to the logo
    moreInfoLink: "#", // Replace with the actual application link
    matches: true
  }
];

const Title = () => {
  return (
    <div className="text-left p-6">
      <h1 className="text-2xl font-bold text-gray-800 md:text-3xl">Check out some of these postings we've found for you 🤗</h1>
    </div>
  );
};

function CapabilityIndicator({ score }) {
  let colorClass = 'text-green-500';
  let title = 'Great match 👍';

  if (score < 70) {
    colorClass = 'text-yellow-300';
    title = 'Fair match ✊';
  } else if (score < 85) {
    colorClass = 'text-lime-300';
    title = 'Good match 👌';
  }

  return (
    <div className={`${colorClass} text-sm flex items-center`}>
      <svg className="w-5 h-5 mr-1" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor">
        <path d="M5 13l4 4L19 7"></path>
      </svg>
      {title}
    </div>
  );
}

export default function JobBoard() {
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		const timer = setTimeout(() => {
		setIsLoading(false);
		}, 5000); // 3000 milliseconds = 3 seconds

		return () => clearTimeout(timer);
	}, []);

	if (isLoading) {
		return (<div className="w-full mt-[40vh]">
			<div className="flex items-center justify-center space-x-2 animate-pulse">
				<div className="w-8 h-8 bg-orange-400 rounded-full"></div>
				<div className="w-8 h-8 bg-orange-500 rounded-full"></div>
				<div className="w-8 h-8 bg-orange-600 rounded-full"></div>
			</div>
			<div className="flex items-center justify-center">
				<div className="mt-3 text-lg font-semibold text-gray-700">
					Finding your perfect fit...
				</div>
			</div>
		</div>)
	}

  return (
	<>
		<Title />
		<div className="bg-gray-100 p-8">
			<div className="grid grid-cols-3 gap-4">
				{jobs.map((job, index) => (
				<div key={index} className="bg-white p-4 shadow-md rounded-lg flex flex-col items-center space-y-2">
					<img src={job.logo} alt={`${job.company} Logo`} className="w-20 h-20 object-contain" />
					<h3 className="text-lg font-semibold">{job.company}</h3>
					<p className="text-sm font-medium">{job.position}</p>
					<p className="text-xs text-center text-gray-600">{job.description}</p>
					<span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
					DEI Score: {job.deiScore}
					</span>
					<a href={job.moreInfoLink} className="text-indigo-600 hover:text-indigo-900 text-sm">Find out more</a>
					<CapabilityIndicator score={job.deiScore} />
				</div>
				))}
			</div>
		</div>
		<div className="text-center mt-4 mb-8">
		<a href="/dashboard" className="inline-block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
			Go Back to Home
		</a>
		</div>
	</>
  );
}