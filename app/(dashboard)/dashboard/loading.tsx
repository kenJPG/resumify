import { DashboardHeader } from "@/components/header"
import { ResumeCreateButton } from "@/components/resume-create-button"
import { ResumeItem } from "@/components/resume-item"
import { DashboardShell } from "@/components/shell"

export default function DashboardLoading() {
  return (
    <DashboardShell>
      <DashboardHeader heading="Resumes" text="Create and manage your resumes.">
        <ResumeCreateButton />
      </DashboardHeader>
      <div className="divide-border-200 divide-y rounded-md border">
        <ResumeItem.Skeleton />
        <ResumeItem.Skeleton />
        <ResumeItem.Skeleton />
        <ResumeItem.Skeleton />
        <ResumeItem.Skeleton />
      </div>
    </DashboardShell>
  )
}
