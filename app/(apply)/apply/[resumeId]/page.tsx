import { notFound, redirect } from "next/navigation"
import { Resume, User } from "@prisma/client"

import { authOptions } from "@/lib/auth"
import { db } from "@/lib/db"
import { getCurrentUser } from "@/lib/session"
import { MainPage } from "@/components/application"

async function getSummary(resumeId: Resume["id"]) {
    const response = await fetch(`http://localhost:3000/api/summary/${resumeId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    })

	let body = response.json()
	console.log(body)
	return body
}

interface EditorPageProps {
  params: { resumeId: string }
}

export default async function ApplicationPage({ params }: EditorPageProps) {
  const user = await getCurrentUser()

  if (!user) {
    redirect(authOptions?.pages?.signIn || "/login")
  }

  const summary = await getSummary(params.resumeId)

  if (!summary) {
    notFound()
  }

  console.log("Summary:", summary)

  return (
	<>
		<MainPage summary={summary} />
	</>
  )
}