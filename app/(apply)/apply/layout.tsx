interface ApplyProps {
  children?: React.ReactNode
}

export default function ApplyLayout({ children }: ApplyProps) {
  return (
    <div className="container mx-auto grid items-start gap-10 py-8">
		{children}
    </div>
  )
}