import { getServerSession } from "next-auth"
import * as z from "zod"

import { authOptions } from "@/lib/auth"
import { db } from "@/lib/db"
import { resumePatchSchema } from "@/lib/validations/resume"
import OpenAI from 'openai';

// Create an OpenAI API client (that's edge friendly!)
const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

// export const runtime = 'edge';
 
const routeContextSchema = z.object({
  params: z.object({
    resumeId: z.string(),
  }),
})

export async function GET(
  req: Request,
  context: z.infer<typeof routeContextSchema>
) {
  try {
    // Validate the route params.
    const { params } = routeContextSchema.parse(context)

    // Check if the user has access to this post.
    if (!(await verifyCurrentUserHasAccessToResume(params.resumeId))) {
      return new Response(null, { status: 403 })
    }

    let resume_data = await db.resume.findFirst({
      where: {
        id: params.resumeId as string,
      },
	  select: {
		title: true,
		content: true
	  }
    })

	let data = JSON.parse(resume_data.content)

	let content = data.blocks.reduce((ac, cu) => {
		return ac + cu.data.text
	}, "")

	let result = content.replace(/\s/gi, " ");

	let res = {}

	let chatCompletion = await openai.chat.completions.create({
		messages: [
			{ role: 'system', content: 'You are to give a grade and provide overall feedback of resumes. For example: "Grade: B | Your resume lacks conciseness. Enhance the highlighted content for a more favourable evaluation, suggested to target Tech/Business Roles that can accomodate disability such as Product Manager, Data Analyst." Keep it succinct, focus on the content, not so much the format. Separate the grade and the content with a bar "|"'},
			{ role: 'user', content: "This is my resume:\n\"\"\"\n" + result + "\n\"\"\""}
		],
		model: 'gpt-3.5-turbo-1106',
	});

	let nice = chatCompletion.choices[0].message.content;
	let [grade, comment] = nice ? nice.split('|') : [undefined, undefined];

	res['grade'] = grade
	res['comment'] = comment
	
	let example = `Disability: Mobility Impairment
	Capabilities (Can do):
	- Demonstrates strong organizational and cognitive skills
	- Excels in roles demanding critical thinking and analysis.
	- Effectively uses assistive tools for communication and task completion.
	- Navigates digital interfaces with ease.

	Limitations (Cannot Do):
	- Engage in physical tasks requiring extensive mobility without assistance.
	- Perform activities that demand prolonged physical movement.
	- Access spaces without proper accommodations for mobility impairment.
	`

	chatCompletion = await openai.chat.completions.create({
		messages: [
			{ role: 'system', content: `You are to analyze the resume of user and respond with an assessment of their capabilities. Use this format as reference: ${example}. Write from the user's perspective.`},
			{ role: 'user', content: "This is my resume:\n\"\"\"\n" + result + "\n\"\"\""}
		],
		model: 'gpt-3.5-turbo-1106',
	});

	res['capability'] = chatCompletion.choices[0].message.content

	chatCompletion = await openai.chat.completions.create({
		messages: [
			{ role: 'system', content: 'You are to analyze the resume of users and respond with a short overview of their "education" and "work" history under 30 words per experience.'},
			{ role: 'user', content: "This is my resume:\n\"\"\"\n" + result + "\n\"\"\""}
		],
		model: 'gpt-3.5-turbo-1106',
	});

	res['experience'] = chatCompletion.choices[0].message.content

    return new Response(JSON.stringify(res), { status: 200 })
  } catch (error) {
	console.log("Error:", error)
    if (error instanceof z.ZodError) {
      return new Response(JSON.stringify(error.issues), { status: 422 })
    }

    return new Response(null, { status: 500 })
  }
}


async function verifyCurrentUserHasAccessToResume(resumeId: string) {
  const session = await getServerSession(authOptions)
  const count = await db.resume.count({
    where: {
      id: resumeId,
      userId: session?.user.id,
    },
  })

  return count > 0
}
