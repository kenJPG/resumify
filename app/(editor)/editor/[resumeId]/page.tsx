import { notFound, redirect } from "next/navigation"
import { Resume, User } from "@prisma/client"

import { authOptions } from "@/lib/auth"
import { db } from "@/lib/db"
import { getCurrentUser } from "@/lib/session"
import { Editor } from "@/components/editor"

async function getResumeForUser(resumeId: Resume["id"], userId: User["id"]) {
  return await db.resume.findFirst({
    where: {
      id: resumeId,
      userId: userId,
    },
  })
}

interface EditorPageProps {
  params: { resumeId: string }
}

export default async function EditorPage({ params }: EditorPageProps) {
  const user = await getCurrentUser()

  if (!user) {
    redirect(authOptions?.pages?.signIn || "/login")
  }

  const resume = await getResumeForUser(params.resumeId, user.id)

  if (!resume) {
    notFound()
  }

  return (
    <Editor
      resume={{
        id: resume.id,
        title: resume.title,
        content: JSON.parse(resume.content)
      }}
    />
  )
}