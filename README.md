## Features

### 1. Landing page
![landing page](screenshots/landing.PNG)

### 2. Login Page
![login page](screenshots/login.PNG)

### 3. Dashboard
![dashboard page](screenshots/dashboard.PNG)

### 4. Editor
![editor page](screenshots/editor.PNG)

### 5. Loading Analysis
![loading page](screenshots/loading.PNG)

### 6. Analysis Results
![analysis page](screenshots/analysis.PNG)

### 7. Finding fitting jobs
![finding page](screenshots/finding.PNG)

### 8. Results page
![results page](screenshots/results.PNG)

## Running Locally

1. Install dependencies using pnpm:

```sh
pnpm install
```

2. Copy `.env.example` to `.env.local` and update the variables.

```sh
cp .env.example .env.local
```

3. Start the development server:

```sh
pnpm dev
```

## License

Licensed under the [MIT license](https://github.com/shadcn/taxonomy/blob/main/LICENSE.md).
